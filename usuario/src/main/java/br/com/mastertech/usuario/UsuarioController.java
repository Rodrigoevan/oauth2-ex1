package br.com.mastertech.usuario;

import br.com.mastertech.usuario.security.UsuarioOauth;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class UsuarioController {
    @PostMapping("/{id}")
    public Usuario create(@PathVariable Integer id, @AuthenticationPrincipal UsuarioOauth usuarioOauth) {
        Usuario usuario = new Usuario();
        usuario.setId(id);
        usuario.setNome(usuarioOauth.getName());
        return usuario;
    }

}
